<?php
/**
 * Created by PhpStorm.
 * User: johnny
 * Date: 19.01.18
 * Time: 15:43
 */

namespace Johnny;

trait HidesAttributes
{
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    //protected $hidden = [];

    /**
     * The attributes that should be visible in serialization.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Get the hidden attributes for the model.
     *
     * @return array
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set the hidden attributes for the model.
     *
     * @param  array  $hidden
     * @return $this
     */
    public function setHidden(array $hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Add hidden attributes for the model.
     *
     * @param  array|string|null  $attributes
     * @return void
     */
    public function addHidden($attributes = null)
    {
        $arr = is_array($attributes) ? $attributes : func_get_args();
        $additional = [];
        foreach($arr as $attribute){
            $fp = explode('.', $attribute, 2);
            if(count($fp) > 1 && array_key_exists($fp[0], $this->relations) && $this->relations[$fp[0]] !== null){
                $this->relations[$fp[0]]->makeHidden($fp[1]);
            } elseif (strpos($fp[0], '*') !== FALSE){
                foreach(array_keys($this->attributes) as $attribute){
                    $pattern = str_ireplace("*", ".*", $fp[0]);
                    if(preg_match("/$pattern/", $attribute))
                        array_push($additional, $attribute);
                }
            }
        }
        $this->hidden = array_unique(array_merge(
            $this->hidden, $arr, $additional
        ));
    }

    /**
     * Get the visible attributes for the model.
     *
     * @return array
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set the visible attributes for the model.
     *
     * @param  array  $visible
     * @return $this
     */
    public function setVisible(array $visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Add visible attributes for the model.
     *
     * @param  array|string|null  $attributes
     * @return void
     */
    public function addVisible($attributes = null)
    {
        $this->visible = array_merge(
            $this->visible, is_array($attributes) ? $attributes : func_get_args()
        );
    }

    /**
     * Make the given, typically hidden, attributes visible.
     *
     * @param  array|string  $attributes
     * @return $this
     */
    public function makeVisible($attributes)
    {
        $this->hidden = array_diff($this->hidden, (array) $attributes);

        if (! empty($this->visible)) {
            $this->addVisible($attributes);
        }

        return $this;
    }

    /**
     * Make the given, typically visible, attributes hidden.
     *
     * @param  array|string  $attributes
     * @return $this
     */
    public function makeHidden($attributes)
    {
        $attributes = (array) $attributes;

        $this->visible = array_diff($this->visible, $attributes);
        $this->addHidden($attributes);
        $this->hidden = array_unique(array_merge($this->hidden, $attributes));

        return $this;
    }
}